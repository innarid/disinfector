# Real-time 2D Multi-Person Pose Estimation on CPU: Lightweight OpenPose

## Requirements

* Python >= 3.6
* PyTorch 0.4.1 (should also work with 1.0, but not tested)

Install requirements `pip install -r requirements.txt`

## Demo

Change PROJECT_PATH in postprocessing/utils.py and run the python demo:
* `python app.py`

## Pedestrian detection
Pre-trained model is available at: https://download.01.org/opencv/openvino_training_extensions/models/human_pose_estimation/checkpoint_iter_370000.pth
Run to get predicrions
* `python demo.py --checkpoint-path checkpoint_iter_370000.pth  --cpu --video  data/video2.mp4`
