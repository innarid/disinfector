from flask import Flask, render_template

from pathlib import Path
import sys

sys.path.append(str(Path(__file__).parent.parent))

from postprocessing.state_wrapper import StateWrapper

app = Flask(__name__)

state_wrapper = StateWrapper('video2.jpg')

def makePretty(stats):
    pretty_stats = {}
    for k, v in stats.items():
        pretty_k = 'Shelf №' + k.split('.')[0].split('_')[1]
        pretty_stats[pretty_k] = v
    return pretty_stats

def makeUgly(name):
    if 'Shelf №' in name:
        return 'video2_{}.png'.format(name.strip('Shelf №'))
    else:
        return name

@app.route('/')
def index():
    filename, stats = state_wrapper.step()
    return render_template('index.html', shelves=makePretty(stats), img=filename)


@app.route('/clear/<string:id>')
def delete(id):
    state_wrapper.clear(makeUgly(id))
    filename, stats = state_wrapper.step()
    return render_template('index.html', shelves=makePretty(stats), img=filename)

if __name__ == '__main__':
    app.run(debug = False)