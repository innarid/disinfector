import operator
import numpy as np
import cv2 as cv

def createDottedImage(shape, radius=5, color=(255, 0, 0), thickness=-1):
    img = np.zeros(shape, np.uint8)
    step = radius * 3
    for i in range(0, shape[1], step):
        for j in range(0, shape[0], step):
            cv.circle(img, (i, j), radius, color, thickness)
    return img

class StateDrawer:
    def __init__(self, state_handler, thresholds = None):
        self.state_handler = state_handler
        self.shape = self.state_handler.base_image.shape
        self.default_thresholds = {
            'yellow': 20.,
            'red': 50.
        }
        self.colors = {
            'yellow': (0, 255, 255),
            'red': (0, 0, 255)
        }
        if thresholds is not None:
            for key in self.default_thresholds.keys():
                assert key in thresholds.keys(), 'please add thresholds for {}'.format(key)
        self.thresholds = thresholds if thresholds is not None else self.default_thresholds
        self.dotted_pattern = {
            'yellow': createDottedImage(self.shape, color=self.colors['yellow']),
            'red':  createDottedImage(self.shape, color=self.colors['red'])
        }
    def draw(self, base=None):
        stats = self.state_handler.getNormedPollutionByMasks()
        img = self.state_handler.base_image.copy() if base is None else base.copy()
        for mask_name, pollution in stats.items():
            mask = self.state_handler.masks[mask_name]
            for color_name, threshold in sorted(self.thresholds.items(), key=operator.itemgetter(1))[::-1]:
                if pollution > threshold:
                    dotted = np.einsum('ijk, ij -> ijk', self.dotted_pattern[color_name], mask)
                    img = cv.addWeighted(img, 1, dotted.astype('float64'), 5, 0)
                    break
        return img

    def getColorStatsByMask(self):
        stats = self.state_handler.getNormedPollutionByMasks()
        result = {}
        for mask_name, pollution in stats.items():
            for color_name, threshold in sorted(self.thresholds.items(), key=operator.itemgetter(1))[::-1]:
                if pollution > threshold:
                    result[mask_name] = color_name
                    break
                else:
                    result[mask_name] = 'green'
        return result