import numpy as np

class PollutionStateHandler:
    def __init__(self, base_image, masks):
        # masks - dict: mask_name -> np.array of type bool
        # base_image - 3d np.array

        masks_shapes = [mask.shape for mask in masks.values()]
        assert (len(dict.fromkeys(masks_shapes)) == 1), 'all masks should have equal shape'
        self.shape = masks_shapes[0]
        assert (len(base_image.shape) == 3), 'expecting 3d array in base_image'
        assert (base_image.shape[:-1] == self.shape), 'shape of masks and base image should be equal, got {0} and {1}'.format(base_image.shape[1:], self.shape)
        self.base_image = base_image
        self.masks = masks
        self.masks_united = np.any(np.array(list(masks.values())), axis=0)
        self.pollution = np.zeros(shape = self.shape).astype('float64')

    def update(self, interraction_mask):
        assert (interraction_mask.shape == self.shape), 'interraction_mask should have shape {}'.format(self.shape)
        self.pollution += interraction_mask * self.masks_united

    def clear(self, mask_name):
        assert mask_name in self.masks, 'unknown mask {}'.format(mask_name)
        mask = self.masks[mask_name]
        self.pollution[mask] = 0

    def clearAll(self):
        self.pollution = np.zeros(shape = self.shape).astype('float64')

    def getPollution(self, mask_name):
        assert mask_name in self.masks, 'unknown mask {}'.format(mask_name)
        mask = self.masks[mask_name]
        return self.pollution * mask

    def getNormedPollutionByMasks(self):
        result = {}
        for name, mask in self.masks.items():
            pollution = np.sum(self.getPollution(name))
            scaling_factor = np.sum(mask.astype('uint64'))
            result[name] = float(pollution) / float(scaling_factor)
        return result

