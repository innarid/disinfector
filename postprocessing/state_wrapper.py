from .utils import prepare_state, loadImagesListAsNp, INTERACTIONS_PATH, FRAMES_PATH, DISPLAY_PATH
from .drawer import StateDrawer
import cv2
import os
import uuid


class StateWrapper:
    def __init__(self, base_image):
        self.state_handler = prepare_state(base_image)
        prefix = base_image.split('.')[0]
        self.interactions = loadImagesListAsNp(path=INTERACTIONS_PATH, prefix=prefix, type='float64')
        self.frames = loadImagesListAsNp(path=FRAMES_PATH, prefix=prefix, type='float64', mode=cv2.IMREAD_COLOR)
        assert (len(self.interactions) == len(self.frames)), 'number of interractions and frames should be equal, got {} and {}'.format(len(self.interactions), len(self.frames))
        self.time = 0
        self.end_of_world = len(self.interactions)
        self.drawer = StateDrawer(self.state_handler)
        self.display_file_name = None

    def stepTime(self):
        if (self.time + 1 == self.end_of_world):
            self.state_handler.clearAll()
            self.time = 0
        else:
            self.time = self.time + 1
        return self.time

    def step(self):
        time = self.stepTime()
        interraction = self.interactions[time]
        frame = self.frames[time]

        self.state_handler.update(interraction)
        img = self.drawer.draw(frame)
        stats = self.drawer.getColorStatsByMask()

        if self.display_file_name is not None:
            os.remove(os.path.join(DISPLAY_PATH, self.display_file_name))
        self.display_file_name = str(uuid.uuid1()) + '.jpg'
        new_file_path = os.path.join(DISPLAY_PATH, self.display_file_name)
        cv2.imwrite(new_file_path, img)
        return self.display_file_name, stats

    def clear(self, shelf_name):
        self.state_handler.clear(shelf_name)