import os
import cv2
import numpy as np
from .state_handler import PollutionStateHandler

PROJECT_PATH = '/home/denis/palaana/repos/other/disinfector'
BASE_PATH = PROJECT_PATH + '/data/'
IMG_PATH = BASE_PATH + 'img/'
TARGET_PATH = BASE_PATH + 'target/'
INTERACTIONS_PATH = BASE_PATH + 'interactions/'
MASKS_PATH = BASE_PATH + 'masks/'
FRAMES_PATH = BASE_PATH + 'frames/'
DISPLAY_PATH = PROJECT_PATH + '/app/static/'

def loadImages(path=MASKS_PATH, prefix=None, mode=cv2.IMREAD_GRAYSCALE):
    images = {}
    for filename in sorted(os.listdir(path)):
        if (prefix is None) or filename.startswith(prefix):
            images[filename] = cv2.imread(os.path.join(path, filename), mode)
    return images

def loadImagesAsNp(path=MASKS_PATH, prefix=None, mode=cv2.IMREAD_GRAYSCALE, type='bool'):
    images = loadImages(path, prefix, mode=mode)
    images_np = {}
    for key, value in images.items():
        images_np[key] = np.array(value, dtype=type)
    return images_np

def loadImagesListAsNp(path=MASKS_PATH, prefix=None, mode=cv2.IMREAD_GRAYSCALE, type='bool'):
    images = []
    for filename in sorted(os.listdir(path)):
        if (prefix is None) or filename.startswith(prefix):
            img = cv2.imread(os.path.join(path, filename), mode)
            images.append(np.array(img, dtype=type))
    return images

def prepare_state(base_image_name):
    base_image = loadImagesAsNp(IMG_PATH, mode=cv2.IMREAD_COLOR, type='uint8')[base_image_name]
    masks_prefix = base_image_name.split('.')[0]
    masks = loadImagesAsNp(prefix=masks_prefix)
    state_handler = PollutionStateHandler(base_image, masks)
    return state_handler