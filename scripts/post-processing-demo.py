from pathlib import Path
import sys

sys.path.append(str(Path(__file__).parent.parent))

from postprocessing.utils import prepare_state, loadImagesAsNp, INTERACTIONS_PATH, TARGET_PATH, FRAMES_PATH, IMG_PATH
from postprocessing.drawer import StateDrawer
import cv2
import os

if __name__ == '__main__':
    base_image_name = 'video2.jpg'
    prefix = base_image_name.split('.')[0]

    interactions = loadImagesAsNp(path=INTERACTIONS_PATH, prefix=prefix, type='float64')

    base_image = cv2.imread(os.path.join(IMG_PATH, base_image_name), cv2.IMREAD_COLOR)

    state_handler = prepare_state(base_image_name)
    drawer = StateDrawer(state_handler)

    for name, mask in interactions.items():
        state_handler.update(mask)
        result = drawer.draw()
        cv2.imwrite(os.path.join(TARGET_PATH, name), result)
    print("OK")